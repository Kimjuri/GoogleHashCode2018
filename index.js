var fs = require('fs');
var process = require('process');

class   Ride {
    constructor(line, id) {
        var fields = line.split(' ');

        this.id = id;
        this.a = parseInt(fields[0], 10);
        this.b = parseInt(fields[1], 10);
        this.x = parseInt(fields[2], 10);
        this.y = parseInt(fields[3], 10);
        this.s = parseInt(fields[4], 10);
        this.f = parseInt(fields[5], 10);
    }
}

class   Dataset {
    constructor(contents) {
        var lines = contents.split('\n');
        var fields = lines[0].split(' ');

        this.R = parseInt(fields[0], 10);
        this.C = parseInt(fields[1], 10);
        this.F = parseInt(fields[2], 10);
        this.N = parseInt(fields[3], 10);
        this.B = parseInt(fields[4], 10);
        this.T = parseInt(fields[5], 10);

        this.rides = []
        for (var i = 0; i < this.N; i++) {
            this.rides.push(new Ride(lines[i+1], i));
        }
    }
}

const FILENAMES = [
    "a_example.in",
    "b_should_be_easy.in",
    "c_no_hurry.in",
    // "d_metropolis.in",
    // "e_high_bonus.in"
];

FILENAMES.forEach(filename => {
    fs.readFile("subject/" + filename, 'ascii', (err, contents) => {
        var dataset = new Dataset(contents);
        computeData(dataset, filename);
    });
})

const WAITING = 0;
const DRIVING = 1;

const dist = function(x1, y1, x2, y2) {
    return Math.abs(x2 - x1) + Math.abs(y2 - y1)
}

const dist_ab = function(a, b) {
    return Math.abs(b.x - a.x) + Math.abs(b.y - b.x)
}

const algo = function(data, cars) {
    data.rides = data.rides.sort((a,b) => a.s - b.s)
    for (var step = 0; step < data.T; step++) {
        // console.log(step + " " + data.T);
        // console.log(data.rides.length + " " + data.rides.filter(ride => ride.f >= step).length)
        if (data.rides.filter(ride => ride.f >= step).length == 0)
            return cars;
        data.rides
            .filter(ride => ride.f >= step)
            .slice(0, cars.length)
            .forEach(ride => {
                const available_cars = cars.filter(car => car.status === WAITING);

                if (available_cars.length > 0) {
                    const sort_cars = available_cars.sort((a,b) => Math.abs(dist_ab(a, ride) - dist_ab(b, ride)))
                    var car = cars.find(car => sort_cars[0].id === car.id)
                    car.status = DRIVING;
                    car.lastRide = ride;
                    car.rides.push(ride);
                    data.rides.splice(data.rides.indexOf(ride), 1);
                }
            })

        // Move cars
        cars.forEach(car => {
            if (car.status === DRIVING) {
                if (car.lastRide.x > car.x)
                    car.x += 1;
                else if (car.lastRide.x < car.x)
                    car.x -= 1;
                else if (car.lastRide.y > car.y)
                    car.y += 1;
                else if (car.lastRide.y < car.y)
                    car.y -= 1;
                else {
                    car.status = WAITING;
                }
            }
        });
    }
    return cars
}

const computeData = function(data, filename) {
    data.rides.sort((a,b) => a.s - b.s)
    var cars = [];
    for (var i = 0; i < data.F; i++) {
        cars.push({
            rides: [],
            status: WAITING,
            id: i,
            x: 0,
            y: 0
        })
    }

    cars = algo(data, cars);

    var output = "";
    cars.forEach(c => {
        output += show_car(c) + "\n";
    });

    fs.writeFile("output_" + filename, output, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
}

const show_car = function(car) {
    var rides = null;
    car.rides.forEach(r => {
        if (rides != null)
            rides += " "
        rides += r.id
    })
    return car.rides.length + " " + rides;
}